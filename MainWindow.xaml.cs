﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Lex_s_Metaverse_Launcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Profile> ProfileList = new List<Profile>();
        bool saveProfilesToFile = true;
        bool firstTime;
        string profilesFileLocation = AppDomain.CurrentDomain.BaseDirectory + "profiles.json";

        private void MainScreen_Loaded(object sender, RoutedEventArgs e)
        {
            //If it's the first time loading the launcher, there's going to be no profiles.json, so when saving profiles, I need to make one first.
            firstTime = !(File.Exists(profilesFileLocation));
            //Most of this is trying to get the profiles.json in to a List of Profile objects so it's easier to work with.
            FileStream fs = File.Open(profilesFileLocation, FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            List<Profile> LoadedProfilesArray;
            try
            {
                JsonSerializerOptions options = new JsonSerializerOptions { IncludeFields = true, WriteIndented = true };
                string testy = sr.ReadToEnd();
                LoadedProfilesArray = JsonSerializer.Deserialize<List<Profile>>(testy, options)!;
                ProfileList.AddRange(LoadedProfilesArray);
            }
            catch (JsonException ex)
            {
                saveProfilesToFile = false;
                MessageBox.Show((firstTime ? "You seem to have no profiles. Please create one!" : ex.Message), "Error! Couldn't load profiles correctly.", MessageBoxButton.OK);
            }
            sr.Close();
            fs.Close();
            Profiles.ItemsSource = ProfileList;
        }

        private void MainScreen_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (saveProfilesToFile | firstTime)
            {
                FileStream fs = File.Open(profilesFileLocation, FileMode.Truncate, FileAccess.Write);
                StreamWriter sr = new StreamWriter(fs);
                JsonSerializerOptions options = new JsonSerializerOptions { WriteIndented = true };
                sr.Write(JsonSerializer.Serialize(ProfileList, options));
                sr.Close();
                fs.Close();
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Profile addedProfile = new Profile("Default", "", "");
            ProfileList.Add(addedProfile);
            Profiles.Items.Refresh();
            Profiles.SelectedItem = addedProfile;
            Profiles.Focus();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if(Profiles.SelectedItem != null)
            {
                ProfileList.Remove((Profile) Profiles.SelectedItem);
                Profiles.Items.Refresh();
                ProfileName.Text = null;
                NeosLocation.Text = null;
                LaunchArguments.Text = null;
            }
        }

        private void Profiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(Profiles.SelectedItem != null)
            {
                ProfileName.Text = ((Profile)Profiles.SelectedItem).Name;
                NeosLocation.Text = ((Profile)Profiles.SelectedItem).NeosLocation;
                LaunchArguments.Text = ((Profile)Profiles.SelectedItem).LaunchArguments;
            }

        }

        private void SaveProfileButton_Click(object sender, RoutedEventArgs e)
        {
            if (Profiles.SelectedItem != null)
            {
                if (!string.IsNullOrWhiteSpace(ProfileName.Text))
                {
                    ((Profile)Profiles.SelectedItem).Name = ProfileName.Text;
                    ((Profile)Profiles.SelectedItem).NeosLocation = NeosLocation.Text;
                    ((Profile)Profiles.SelectedItem).LaunchArguments = LaunchArguments.Text;
                    Profiles.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("Profile Name cannot be empty or only whitespace.", "Profile Error!");
                }
            }
            else
            {
                MessageBox.Show("Please select a profile first.");
            }
 
        }

        private void BrowseLocationButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Programs|*.exe";
            openFileDialog.DefaultExt = ".exe";
            bool? result = openFileDialog.ShowDialog();
            if (result == true)
            {
                NeosLocation.Text = openFileDialog.FileName;
            }
        }

        private void LaunchButton_Click(object sender, RoutedEventArgs e)
        {
            if (Profiles.SelectedItem != null)
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(((Profile)Profiles.SelectedItem).LaunchArguments))
                    {
                        Process.Start(((Profile)Profiles.SelectedItem).NeosLocation);
                    }
                    else
                    {
                        Process.Start(((Profile) Profiles.SelectedItem).NeosLocation, ((Profile) Profiles.SelectedItem).LaunchArguments);
                    }
                }
                catch (Win32Exception ex)
                {
                    MessageBox.Show(string.Format("There was a problem launching {0}", ((Profile)Profiles.SelectedItem).NeosLocation));
                }
            }
            else
            {
                MessageBox.Show("Please select a profile first.");
            }
        }
    }
}
