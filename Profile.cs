﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;

namespace Lex_s_Metaverse_Launcher
{
    internal class Profile
    {
        [JsonPropertyName ("name")]
        public string Name { get; set; }

        [JsonPropertyName ("neosLocation")]
        public string NeosLocation { get; set; }

        [JsonPropertyName ("launchArguments")]
        public string LaunchArguments { get; set; }

        [JsonConstructor]
        public Profile(string name, string neosLocation, string launchArguments)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("There seems to be a problem loading your profiles.", "Error");
            }

            Name = name;
            NeosLocation = neosLocation;
            LaunchArguments = launchArguments;
        }
    }
}
